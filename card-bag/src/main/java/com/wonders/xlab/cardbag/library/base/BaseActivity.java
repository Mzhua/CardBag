package com.wonders.xlab.cardbag.library.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.wonders.xlab.cardbag.library.ComponentHolder;
import com.wonders.xlab.cardbag.library.di.AppComponent;
import com.wonders.xlab.cardbag.library.util.LogUtil;


/**
 * Created by hua on 16/7/19.
 * 一个Presenter对应一个View,如果一个View需要多个Presenter了,则需要考虑设计是否合理了,或者是不是Presenter应该作为一个Module
 */

public abstract class BaseActivity<P extends BaseContract.Presenter> extends AppCompatActivity {
    private P mIBasePresenter;

    public abstract P getPresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIBasePresenter = getPresenter();
        if (null != mIBasePresenter) {
            mIBasePresenter.onAttachView();
        } else {
            LogUtil.warning("BaseActivity", "you have to initial the presenter before the BaseActivity Create");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null != mIBasePresenter) {
            mIBasePresenter.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mIBasePresenter) {
            mIBasePresenter.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mIBasePresenter) {
            mIBasePresenter.onDetachView();
        }
    }

    protected AppComponent getAppComponent() {
        return ComponentHolder.getAppComponent();
    }
}
