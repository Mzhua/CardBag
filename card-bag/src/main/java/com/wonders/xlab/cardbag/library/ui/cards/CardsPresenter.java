package com.wonders.xlab.cardbag.library.ui.cards;

import com.wonders.xlab.cardbag.library.base.BaseContract;
import com.wonders.xlab.cardbag.library.base.BasePresenter;
import com.wonders.xlab.cardbag.library.base.DefaultSubscriber;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by hua on 16/8/9.
 */
public class CardsPresenter extends BasePresenter implements CardsContract.Presenter {
    private CardsModel mCardsModel;
    private CardsContract.View mView;

    @Inject
    public CardsPresenter(CardsModel cardsModel, CardsContract.View view) {
        mCardsModel = cardsModel;
        mView = view;
        addModels(mCardsModel);
    }

    @Override
    public void getCards() {
        mCardsModel.execute(new DefaultSubscriber<Response<CardsEntity>>() {
            @Override
            public BaseContract.View getView() {
                return mView;
            }

            @Override
            public void onNext(Response<CardsEntity> cardsEntityResponse) {
                mView.showToastMessage(cardsEntityResponse.body().getResults().get(0).getCard_name());
            }
        });
    }
}
