package com.wonders.xlab.cardbag.library.ui.scanner;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.Toast;

import com.wonders.xlab.cardbag.library.R;
import com.wonders.xlab.cardbag.library.util.Encoder;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zxing.ZXingView;


public class CBScannerActivity extends AppCompatActivity implements QRCodeView.Delegate {
    private ZXingView mZXingView;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanner_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mZXingView = (ZXingView) findViewById(R.id.zxingview);
        mImageView = (ImageView) findViewById(R.id.cbscanner_iv);
        mZXingView.setDelegate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mZXingView.startCamera();
        mZXingView.startSpotAndShowRect();
    }

    @Override
    protected void onStop() {
        mZXingView.stopCamera();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mZXingView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        mZXingView.startSpot();
        Encoder ecc = new Encoder(mImageView.getWidth(), mImageView.getHeight());
        try {
            Bitmap bitm = ecc.barcode(result);
            mImageView.setImageBitmap(bitm);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onScanQRCodeOpenCameraError() {
        Toast.makeText(this, "开启相机出错,请检查相关权限!", Toast.LENGTH_SHORT).show();
    }

}
