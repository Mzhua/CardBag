package com.wonders.xlab.cardbag.library.di;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by hua on 16/8/8.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    Retrofit getRetrofit();
}
