package com.wonders.xlab.cardbag.library.ui.cards;

import com.wonders.xlab.cardbag.library.base.BaseModel;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by hua on 16/8/9.
 */
public class CardsModel extends BaseModel<CardsEntity>{
    private CardsAPI mCardsAPI;

    @Inject
    public CardsModel(CardsAPI cardsAPI) {
        mCardsAPI = cardsAPI;
    }

    @Override
    protected Observable<Response<CardsEntity>> buildRequestObservable(java.lang.Object... params) {
        return mCardsAPI.getCards();
    }
}
