package com.wonders.xlab.cardbag.library;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.wonders.xlab.cardbag.library.di.AppModule;
import com.wonders.xlab.cardbag.library.di.DaggerAppComponent;
import com.wonders.xlab.cardbag.library.ui.home.CBHomeActivity;

/**
 * Created by hua on 16/8/10.
 */
public class CBag {
    private static final String EXTRA_PREFIX = BuildConfig.APPLICATION_ID;
    private static final Object object = new Object();
    private static CBag cBag;
    private Bundle mCBagOptionsBundle;

    private CBag() {
        mCBagOptionsBundle = new Bundle();
    }

    /**
     * have to call the method before call start
     *
     * @param application
     */
    public static CBag init(Application application) {
        if (cBag == null) {
            synchronized (object) {
                if (cBag == null) {
                    ComponentHolder.setAppComponent(DaggerAppComponent.builder()
                            .appModule(new AppModule(application))
                            .build());
                    cBag = new CBag();
                }
            }
        }

        return cBag;
    }

    /**
     * start @{@link CBHomeActivity}
     *
     * @param activity
     */
    public CBag start(Activity activity) {
        activity.startActivity(new Intent(activity, CBHomeActivity.class));
        return this;
    }

    /**
     * start @{@link CBHomeActivity}
     *
     * @param fragment
     */
    public CBag start(Fragment fragment) {
        fragment.startActivity(new Intent(fragment.getContext(), CBHomeActivity.class));
        return this;
    }

    public CBag withOption(Options option) {
        mCBagOptionsBundle.putAll(option.getOptionBundle());
        return this;
    }

    /**
     *
     */
    public static class Options{
        private static final String EXTRA_COLOR_PRIMARY = EXTRA_PREFIX + ".ColorPrimary";
        private static final String EXTRA_COLOR_PRIMARY_DARK = EXTRA_PREFIX + ".ColorPrimaryDark";

        private final Bundle mOptionBundle;

        public Options() {
            mOptionBundle = new Bundle();
        }

        @NonNull
        public Bundle getOptionBundle() {
            return mOptionBundle;
        }

        public void setColorPrimary(@ColorInt int colorPrimary) {
            mOptionBundle.putInt(EXTRA_COLOR_PRIMARY,colorPrimary);
        }

        public void setColorPrimaryDark(@ColorInt int colorPrimaryDark) {
            mOptionBundle.putInt(EXTRA_COLOR_PRIMARY_DARK,colorPrimaryDark);
        }
    }
}
