package com.wonders.xlab.cardbag.library.ui.cards;

import com.wonders.xlab.cardbag.library.di.AppComponent;
import com.wonders.xlab.cardbag.library.di.scop.ActivityScope;

import dagger.Component;

/**
 * Created by hua on 16/8/9.
 */
@ActivityScope
@Component(dependencies = AppComponent.class,modules = CardsModule.class)
public interface CardsComponent {
    void inject(CBCardsActivity activity);
}
