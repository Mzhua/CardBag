package com.wonders.xlab.cardbag.library.base;

import rx.Subscriber;

/**
 * Created by hua on 16/8/11.
 */
public abstract class DefaultSubscriber<T> extends Subscriber<T> {
    public abstract BaseContract.View getView();

    /**
     * override this method to change the default behavior when error occur
     * @return
     */
    public boolean showErrorToast() {
        return true;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        if (getView() != null && showErrorToast()) {
            getView().showToastMessage(e.getMessage());
        }
    }
}
