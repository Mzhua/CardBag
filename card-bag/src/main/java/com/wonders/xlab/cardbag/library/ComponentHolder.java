package com.wonders.xlab.cardbag.library;


import com.wonders.xlab.cardbag.library.di.AppComponent;

/**
 * Created by hua on 16/8/4.
 */

public class ComponentHolder {
    private static AppComponent mAppComponent = null;

    public static void setAppComponent(AppComponent appComponent) {
        mAppComponent = appComponent;
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }
}
