package com.wonders.xlab.cardbag.library.base;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.wonders.xlab.cardbag.library.ComponentHolder;
import com.wonders.xlab.cardbag.library.di.AppComponent;
import com.wonders.xlab.cardbag.library.util.LogUtil;

/**
 * Created by hua on 16/7/19.
 */

public abstract class BaseFragment<P extends BaseContract.Presenter> extends Fragment {
    private P mIBasePresenter;

    public abstract P getPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIBasePresenter = getPresenter();
        if (null != mIBasePresenter) {
            mIBasePresenter.onAttachView();
        } else {
            LogUtil.warning("BaseFragment", "you have to initial the presenter before the BaseFragment Create");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (null != mIBasePresenter) {
            mIBasePresenter.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (null != mIBasePresenter) {
            mIBasePresenter.onStop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mIBasePresenter) {
            mIBasePresenter.onDetachView();
        }
    }

    protected AppComponent getAppComponent() {
        return ComponentHolder.getAppComponent();
    }
}
