package com.wonders.xlab.cardbag.library.util;

import android.util.Log;

/**
 * Created by hua on 16/8/1.
 * Log util
 */

public class LogUtil {
    public static void error(String tag, String message) {
        Log.e(tag, message);
    }

    public static void debug(String tag, String message) {
        Log.d(tag, message);
    }

    public static void warning(String tag, String message) {
        Log.w(tag, message);
    }

    public static void info(String tag, String message) {
        Log.i(tag, message);
    }
}
