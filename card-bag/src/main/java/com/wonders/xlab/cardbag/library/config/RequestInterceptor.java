package com.wonders.xlab.cardbag.library.config;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by hua on 16/8/12.
 * add header for all request
 */
public class RequestInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header("X-LC-Id", "27bHde5iXz1tTpQmbwRcenxg-gzGzoHsz")
                .header("X-LC-Key", "THX77SeFRWJpGzL4QXwlWSCQ")
                .header("Content-Type", "application/json")
                .method(original.method(), original.body())
                .build();

        return chain.proceed(request);
    }
}
