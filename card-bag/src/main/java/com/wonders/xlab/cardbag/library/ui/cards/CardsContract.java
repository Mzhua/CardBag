package com.wonders.xlab.cardbag.library.ui.cards;

import com.wonders.xlab.cardbag.library.base.BaseContract;

/**
 * Created by hua on 16/8/9.
 */
public interface CardsContract {
    interface Presenter extends BaseContract.Presenter{
        void getCards();
    }

    interface View extends BaseContract.View{
    }
}
