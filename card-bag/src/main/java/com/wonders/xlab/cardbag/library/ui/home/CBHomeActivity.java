package com.wonders.xlab.cardbag.library.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.wonders.xlab.cardbag.library.R;
import com.wonders.xlab.cardbag.library.ui.cards.CBCardsActivity;
import com.wonders.xlab.cardbag.library.ui.scanner.CBScannerActivity;

public class CBHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cb_home_activity);
    }

    public void useCard(View view) {
        startActivity(new Intent(this, CBScannerActivity.class));
    }

    public void manageCard(View view) {
        startActivity(new Intent(this, CBCardsActivity.class));
    }
}
