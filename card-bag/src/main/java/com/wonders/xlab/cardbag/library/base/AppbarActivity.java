package com.wonders.xlab.cardbag.library.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.wonders.xlab.cardbag.library.R;

/**
 * Created by hua on 16/8/9.
 */
public abstract class AppbarActivity<P extends BaseContract.Presenter> extends BaseActivity<P> {

    public abstract int getLayoutId();

    public abstract String getToolbarTitle();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            throw new NullPointerException("you have to include app_bar.xml in your layout or add a toolbar manual and it's id must be toolbar");
        }
        toolbar.setTitle(TextUtils.isEmpty(getToolbarTitle()) ? getString(R.string.library_name) : getToolbarTitle());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
