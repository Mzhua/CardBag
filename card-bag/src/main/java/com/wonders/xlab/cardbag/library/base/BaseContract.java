package com.wonders.xlab.cardbag.library.base;

/**
 * Created by hua on 16/8/4.
 */
public interface BaseContract {
    interface Presenter {
        void onAttachView();

        void onStart();

        void onStop();

        void onDetachView();
    }

    interface View {
        void showToastMessage(String message);
    }

}
