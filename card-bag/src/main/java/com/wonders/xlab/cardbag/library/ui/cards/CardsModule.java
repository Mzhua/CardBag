package com.wonders.xlab.cardbag.library.ui.cards;

import com.wonders.xlab.cardbag.library.di.scop.ActivityScope;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by hua on 16/8/9.
 */
@Module
public class CardsModule {
    private CardsContract.View mView;

    public CardsModule(CardsContract.View view) {
        mView = view;
    }

    @Provides
    @ActivityScope
    CardsAPI provideCardsAPI(Retrofit retrofit) {
        return retrofit.create(CardsAPI.class);
    }

    @Provides
    @ActivityScope
    CardsContract.View provideView() {
        return mView;
    }
}
