package com.wonders.xlab.cardbag.library.base;


import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by hua on 16/7/19.
 * you should call method @{@link #addModels(BaseModel[])} to relate your model lifecycle with presenter, so the model request cancel auto canceled when the view is gone
 */

public abstract class BasePresenter implements BaseContract.Presenter{
    private ArrayList<BaseModel> mModels;

    protected void addModels(BaseModel... models) {
        if (mModels == null) {
            mModels = new ArrayList<>();
        }
        Collections.addAll(mModels, models);
    }

    @Override
    public void onAttachView() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDetachView() {
        if (mModels != null) {
            for (BaseModel model : mModels) {
                if (model != null) {
                    model.unSubscribe();
                }
            }
            mModels.clear();
        }
    }

}
