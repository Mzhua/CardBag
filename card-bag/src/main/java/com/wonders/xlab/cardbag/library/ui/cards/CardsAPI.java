package com.wonders.xlab.cardbag.library.ui.cards;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by hua on 16/8/9.
 */
public interface CardsAPI {
    @GET("classes/cards")
    Observable<Response<CardsEntity>> getCards();
}
