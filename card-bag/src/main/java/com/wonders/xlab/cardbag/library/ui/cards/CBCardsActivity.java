package com.wonders.xlab.cardbag.library.ui.cards;

import android.os.Bundle;
import android.widget.Toast;

import com.wonders.xlab.cardbag.library.R;
import com.wonders.xlab.cardbag.library.base.AppbarActivity;

import javax.inject.Inject;


public class CBCardsActivity extends AppbarActivity<CardsContract.Presenter> implements CardsContract.View {

    @Inject
    CardsPresenter mPresenter;

    @Override
    public CardsContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    public int getLayoutId() {
        return R.layout.cards_activity;
    }

    @Override
    public String getToolbarTitle() {
        return getString(R.string.title_cards);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DaggerCardsComponent.builder()
                .appComponent(getAppComponent())
                .cardsModule(new CardsModule(this))
                .build()
                .inject(this);
        super.onCreate(savedInstanceState);

        mPresenter.getCards();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
