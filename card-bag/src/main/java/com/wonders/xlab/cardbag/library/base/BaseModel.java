package com.wonders.xlab.cardbag.library.base;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by hua on 16/7/19.
 */

public abstract class BaseModel<E extends BaseEntity> {

    private Subscription mSubscription;

    protected abstract Observable<Response<E>> buildRequestObservable(Object... params);

    /**
     *
     * @param subscriber
     * @param params params for @{@link #buildRequestObservable(Object...)}
     */
    public void execute(DefaultSubscriber<Response<E>> subscriber,Object... params) {
        mSubscription = buildRequestObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    /**
     * cancel the request
     */
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }
}
